/****************************************************
 * Description: Service for t_mall_thanks
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.service;
import com.xjj.mall.common.pojo.DataTablesResult;
import com.xjj.mall.entity.ThanksEntity;


import com.xjj.framework.service.XjjService;

public interface ThanksService  extends XjjService<ThanksEntity>{
	
	/**
     * 分页获取捐赠列表
     * @param page
     * @param size
     * @return
     */
    DataTablesResult getThanksListByPage(int page, int size);
}
