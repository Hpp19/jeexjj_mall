package com.xjj.mall.pojo;

import java.io.Serializable;
import java.util.List;

import com.xjj.mall.entity.OrderEntity;
import com.xjj.mall.entity.OrderItemEntity;
import com.xjj.mall.entity.OrderShippingEntity;

/**
 * @author zhanghejie
 */
public class OrderDetail implements Serializable {

    private OrderEntity tbOrder;

    private List<OrderItemEntity> tbOrderItem;

    private OrderShippingEntity tbOrderShipping;

    public List<OrderItemEntity> getTbOrderItem() {
        return tbOrderItem;
    }

    public void setTbOrderItem(List<OrderItemEntity> tbOrderItem) {
        this.tbOrderItem = tbOrderItem;
    }

    public OrderEntity getTbOrder() {
        return tbOrder;
    }

    public void setTbOrder(OrderEntity tbOrder) {
        this.tbOrder = tbOrder;
    }

    public OrderShippingEntity getTbOrderShipping() {
        return tbOrderShipping;
    }

    public void setTbOrderShipping(OrderShippingEntity tbOrderShipping) {
        this.tbOrderShipping = tbOrderShipping;
    }
}
