/****************************************************
 * Description: Entity for t_mall_order_shipping
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class OrderShippingEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public OrderShippingEntity(){}
    private String orderId;//订单ID
    private String receiverName;//收货人全名
    private String receiverPhone;//固定电话
    private String receiverMobile;//移动电话
    private String receiverState;//省份
    private String receiverCity;//城市
    private String receiverDistrict;//区/县
    private String receiverAddress;//收货地址，如：xx路xx号
    private String receiverZip;//邮政编码,如：310001
    private Date created;//created
    private Date updated;//updated
    /**
     * 返回订单ID
     * @return 订单ID
     */
    public String getOrderId() {
        return orderId;
    }
    
    /**
     * 设置订单ID
     * @param orderId 订单ID
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    /**
     * 返回收货人全名
     * @return 收货人全名
     */
    public String getReceiverName() {
        return receiverName;
    }
    
    /**
     * 设置收货人全名
     * @param receiverName 收货人全名
     */
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
    
    /**
     * 返回固定电话
     * @return 固定电话
     */
    public String getReceiverPhone() {
        return receiverPhone;
    }
    
    /**
     * 设置固定电话
     * @param receiverPhone 固定电话
     */
    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }
    
    /**
     * 返回移动电话
     * @return 移动电话
     */
    public String getReceiverMobile() {
        return receiverMobile;
    }
    
    /**
     * 设置移动电话
     * @param receiverMobile 移动电话
     */
    public void setReceiverMobile(String receiverMobile) {
        this.receiverMobile = receiverMobile;
    }
    
    /**
     * 返回省份
     * @return 省份
     */
    public String getReceiverState() {
        return receiverState;
    }
    
    /**
     * 设置省份
     * @param receiverState 省份
     */
    public void setReceiverState(String receiverState) {
        this.receiverState = receiverState;
    }
    
    /**
     * 返回城市
     * @return 城市
     */
    public String getReceiverCity() {
        return receiverCity;
    }
    
    /**
     * 设置城市
     * @param receiverCity 城市
     */
    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity;
    }
    
    /**
     * 返回区/县
     * @return 区/县
     */
    public String getReceiverDistrict() {
        return receiverDistrict;
    }
    
    /**
     * 设置区/县
     * @param receiverDistrict 区/县
     */
    public void setReceiverDistrict(String receiverDistrict) {
        this.receiverDistrict = receiverDistrict;
    }
    
    /**
     * 返回收货地址，如：xx路xx号
     * @return 收货地址，如：xx路xx号
     */
    public String getReceiverAddress() {
        return receiverAddress;
    }
    
    /**
     * 设置收货地址，如：xx路xx号
     * @param receiverAddress 收货地址，如：xx路xx号
     */
    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }
    
    /**
     * 返回邮政编码,如：310001
     * @return 邮政编码,如：310001
     */
    public String getReceiverZip() {
        return receiverZip;
    }
    
    /**
     * 设置邮政编码,如：310001
     * @param receiverZip 邮政编码,如：310001
     */
    public void setReceiverZip(String receiverZip) {
        this.receiverZip = receiverZip;
    }
    
    /**
     * 返回created
     * @return created
     */
    public Date getCreated() {
        return created;
    }
    
    /**
     * 设置created
     * @param created created
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    
    /**
     * 返回updated
     * @return updated
     */
    public Date getUpdated() {
        return updated;
    }
    
    /**
     * 设置updated
     * @param updated updated
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.OrderShippingEntity").append("ID="+this.getId()).toString();
    }
}

