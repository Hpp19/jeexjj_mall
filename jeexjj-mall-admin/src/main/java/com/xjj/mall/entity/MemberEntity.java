/****************************************************
 * Description: Entity for 用户表
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import java.math.BigDecimal;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class MemberEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public MemberEntity(){}
    private String username;//用户名
    private String password;//密码，加密存储
    private String phone;//注册手机号
    private String email;//注册邮箱
    private Date created;//created
    private Date updated;//updated
    private String sex;//sex
    private String address;//address
    private Integer state;//state
    private String file;//头像
    private String description;//description
    private Integer points;//积分
    private BigDecimal balance;//余额
    /**
     * 返回用户名
     * @return 用户名
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * 设置用户名
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * 返回密码，加密存储
     * @return 密码，加密存储
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * 设置密码，加密存储
     * @param password 密码，加密存储
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * 返回注册手机号
     * @return 注册手机号
     */
    public String getPhone() {
        return phone;
    }
    
    /**
     * 设置注册手机号
     * @param phone 注册手机号
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    /**
     * 返回注册邮箱
     * @return 注册邮箱
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * 设置注册邮箱
     * @param email 注册邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * 返回created
     * @return created
     */
    public Date getCreated() {
        return created;
    }
    
    /**
     * 设置created
     * @param created created
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    
    /**
     * 返回updated
     * @return updated
     */
    public Date getUpdated() {
        return updated;
    }
    
    /**
     * 设置updated
     * @param updated updated
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    /**
     * 返回sex
     * @return sex
     */
    public String getSex() {
        return sex;
    }
    
    /**
     * 设置sex
     * @param sex sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }
    
    /**
     * 返回address
     * @return address
     */
    public String getAddress() {
        return address;
    }
    
    /**
     * 设置address
     * @param address address
     */
    public void setAddress(String address) {
        this.address = address;
    }
    
    /**
     * 返回state
     * @return state
     */
    public Integer getState() {
        return state;
    }
    
    /**
     * 设置state
     * @param state state
     */
    public void setState(Integer state) {
        this.state = state;
    }
    
    /**
     * 返回头像
     * @return 头像
     */
    public String getFile() {
        return file;
    }
    
    /**
     * 设置头像
     * @param file 头像
     */
    public void setFile(String file) {
        this.file = file;
    }
    
    /**
     * 返回description
     * @return description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * 设置description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * 返回积分
     * @return 积分
     */
    public Integer getPoints() {
        return points;
    }
    
    /**
     * 设置积分
     * @param points 积分
     */
    public void setPoints(Integer points) {
        this.points = points;
    }
    
    /**
     * 返回余额
     * @return 余额
     */
    public BigDecimal getBalance() {
        return balance;
    }
    
    /**
     * 设置余额
     * @param balance 余额
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.MemberEntity").append("ID="+this.getId()).toString();
    }
}

