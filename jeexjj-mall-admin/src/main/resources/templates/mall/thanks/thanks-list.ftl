<#--
/****************************************************
 * Description: t_mall_thanks的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>nick_name</th>
	        <th>username</th>
	        <th>money</th>
	        <th>info</th>
	        <th>通知邮箱</th>
	        <th>状态 0待审核 1确认显示  2驳回 3通过不展示</th>
	        <th>支付方式</th>
	        <th>关联订单id</th>
	        <th>捐赠日期</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.nickName}
			</td>
			<td>
			    ${item.username}
			</td>
			<td>
			    ${item.money}
			</td>
			<td>
			    ${item.info}
			</td>
			<td>
			    ${item.email}
			</td>
			<td>
			    ${item.state}
			</td>
			<td>
			    ${item.payType}
			</td>
			<td>
			    ${item.orderId}
			</td>
			<td>
			    ${item.createDate?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/thanks/input/${item.id}','修改t_mall_thanks','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/thanks/delete/${item.id}','删除t_mall_thanks？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>